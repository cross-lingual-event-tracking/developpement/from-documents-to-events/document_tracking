- **Multi-language implementation**
    - 2 steps: monolingual clustering + multilingual clustering of already monolingual clusters
    - 1 step: all-in-one process by considering all documents without taking the language into account (requires documents to be represented using comparable vector sparse, regardless of language.

- Baseline implementations
    - **newsLens**:
        Laban, Philippe, et Marti Hearst. 2017. « NewsLens: Building and Visualizing Long-Ranging News
        Stories ». In Proceedings of the Events and Stories in the News Workshop, 1‑9. Vancouver,
        Canada: Association for Computational Linguistics. https://doi.org/10.18653/v1/W17-2701.

    - **Mathis Linger et al. (2020) newsLens implementation**:
        Linger, Mathis, et Mhamed Hajaiej. 2020. « Batch Clustering for Multilingual News Streaming ».
        In Proceedings of Text2Story - Third Workshop on Narrative Extraction From Texts Co-Located with
        42nd European Conference on Information Retrieval, 2593:55‑61. CEUR Workshop Proceedings. Lisbon,
        Portugal. http://ceur-ws.org/Vol-2593/paper7.pdf.
